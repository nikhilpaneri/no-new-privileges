= No new privileges

//tag::abstract[]

By default, processes in the container can gain more privileges
for example through `suid` binaries.


//end::abstract[]

A malicious container with `suid` binary can run processes
as root, even though the container run by a low privileged user.

//tag::lab[]

== Lab

Your objective in each lab is to pass the build.


=== Task 0

Fork and clone this repository.
Install `docker`, `docker-compose` and `make` on your system.

. Run unit tests: `make test`. 
. Build the program: `make build`.
. Run it: `make run`.
. Run security tests: `make securitytest`.

Note: The last test will fail. 


=== Task 1

Review `Dockerfile` and `docker-compose.yml` file. 
The container runs as a low privileged user, however,
The content of root owned file `/etc/shadow` can be read.
Find out the security issue and why tests fails.

Note: Avoid looking at tests and try to
spot the vulnerability on your own.

=== Task 2

The `/bin/supercat` is a `suid` binary and effectively run as a root when
it is called by a normal user. 
How we can prevent the processes inside the container
gain additional privileges even through `suid` binaries?

This security issue can be fixed without touching the `Dockerfile`
This time when you run the container, you
should not be able to see content of `/etc/shadow`.


== Task 3

Run security tests again. Make sure build will pass.
If you stuck, move to the next task.

=== Task 4 
 
Check out the `patch` branch and review the changes. 
Run all tests and make sure everything pass. 


=== Task 5 
 
Merge the patch branch to master. 
Commit and push your changes. 
Does pipeline for your forked repository show that you have passed the build?   

(Note: you do NOT need to send a pull request)

//end:lab[]

//tag::references[]

== References

* https://raesene.github.io/blog/2019/06/01/docker-capabilities-and-no-new-privs/

//end::references[]
